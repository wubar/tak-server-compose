# TAK Server Compose
Scripts and docker-compose to streamline setup for TAK server

## Quickstart (the TL;DR version)
These scripts assume you don't need to sudo for `docker` and `docker-compose`. Run as root if needed.
See https://docs.docker.com/engine/install/linux-postinstall/ for details.

Clone this repo and run from the project root depending on your CPU architecture.

### AMD64
```
. scripts/setup.sh
```

### ARM64 (32-bit isn't supported)
```
. scripts/setup.sh arm
```
Configure the rest through http://localhost:8080

## Verifying Checksum
The script will automatically verify the checksum of the package matches tak.gov releases. Please conduct your own due diligence when 
downloading the package. Further reading encouraged at https://itsfoss.com/checksum-tools-guide-linux/

## The Details
This project seeks to streamline the instructions in TAK server's offical docs to build and configure TAK server using Docker.

The setup script will populate config files, start up TAK server and the POSTGRES database via docker-compose, and generate the required certs.

Admin user configuration may fail if the TAK server hasn't finished starting up. If this happens, you'll need to re-run the setup script or just lines 64 and 65.

## Contributing
Please feel free to open merge requests. The setup script can definitely be improved at the the very least.

## Authors and acknowledgment
Shoutout to the TAK server team for creating the core project and open-sourcing it!