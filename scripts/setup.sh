### Vars
release=takserver-docker-4.6-RELEASE-26
checksum=7ca58221b8d35d40df906144c5834e6d9fa85b47
downloadurl=https://gitlab.com/wubar/tak-server-compose/-/package_files/46470870/download
composefile=docker-compose.yml
timeout=60
takservername=tak

if [ "$1" == "arm" ]
then
    timeout=250
    composefile=docker-compose.arm.yml
    takservername=tak-arm
fi

## Download the docker release
echo "$checksum $release.zip"  > $release.zip.sha1
curl $downloadurl -o $release.zip

echo "Verifying checksum for release $release is $checksum."
sha1sum -c $release.zip.sha1
if [ $? -ne 0 ]
then
    echo "Failed to verify checksum. Aborting."
    return 1
fi

## Set up directory structure
unzip $release.zip -d /tmp/takserver
mv -f /tmp/takserver/$release/tak .
clear

## Set admin username and password
read -p "Admin Username:" user
read -sp 'Admin Password (15 characters including 1 uppercase, 1 lowercase, 1 number, and 1 special character):' password
printf "\n"

## Set postgres password
read -sp 'POSTGRES Password:' pgpassword
printf "\n"
sed -i "s/password=\".*\"/password=\"${pgpassword}\"/" tak/CoreConfig.xml

## Set variables for generating CA and client certs
read -p "State (for cert generation):" state
read -p "City (for cert generation):" city
read -p "Organizational Unit (for cert generation):" orgunit

# Writes variables to a .env file for docker-compose
cat << EOF > .env
STATE=$state
CITY=$city
ORGANIZATIONAL_UNIT=$orgunit
EOF

## Runs through setup
docker-compose --file $composefile up -d
docker-compose --file $composefile exec $takservername bash -c "cd /opt/tak/certs && ./makeRootCa.sh"
docker-compose --file $composefile exec $takservername bash -c "cd /opt/tak/certs && ./makeCert.sh server takserver"
docker-compose --file $composefile exec $takservername bash -c "cd /opt/tak/certs && ./makeCert.sh client $user"
docker-compose --file $composefile stop $takservername
docker-compose --file $composefile start $takservername
echo "waiting $timeout seconds for TAK server to go live again (this may take longer on slower machines)"
sleep $timeout
docker-compose --file $composefile exec $takservername bash -c "cd /opt/tak/ && java -jar /opt/tak/utils/UserManager.jar usermod -A -p $password $user"
docker-compose --file $composefile exec $takservername bash -c "cd /opt/tak/ && java -jar utils/UserManager.jar certmod -A certs/files/$user.pem"

echo "If everything ran successfully, you should be able to hit the http address at http://localhost:8080 and configure TAK server the rest of the way."
echo "You should probably remove the port 8080:8080 mapping in docker-compose.yml to secure the server afterwards."
echo "Admin user certs should be under ./tak/certs/files"